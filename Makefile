.PHONY: clean all reg.shasum

all: reg

reg: reg.shasum
	curl -Lso reg-linux-amd64 $$(curl -sL https://api.github.com/repos/genuinetools/reg/releases/latest | jq -r '.assets[] | select(.name | test("reg-linux-amd64")).browser_download_url')
	sha256sum -c reg.shasum
	chmod +x reg-linux-amd64

reg.shasum:
	curl -Ls $$(curl -sL https://api.github.com/repos/genuinetools/reg/releases/latest | jq -r '.assets[] | select(.name | test("reg-linux-amd64.sha256")).browser_download_url') | sed -e 's|/github/workspace/cross/||' > reg.shasum

clean:
	rm -rf reg-linux-amd64 reg.shasum
