# Buildah image for Goozbach

This image extends https://quay.io/repository/buildah/stable

This enables building via gitlab.com gitlab-ci based on work done by Major Hayden

* https://major.io/2019/05/24/build-containers-in-gitlab-ci-with-buildah/
* https://major.io/2019/08/13/buildah-error-vfs-driver-does-not-support-overlay-mountopt-options/


